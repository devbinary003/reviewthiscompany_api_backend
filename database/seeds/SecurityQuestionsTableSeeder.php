<?php

use Illuminate\Database\Seeder;

class SecurityQuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('securityques')->insert([
            'title' => 'Childhood Nickname',
            'description' => 'What was your childhood nickname?',
        ]);
        DB::table('securityques')->insert([
            'title' => 'Childhood Friend',
            'description' => 'What is the name of your favorite childhood friend?',
        ]);
        DB::table('securityques')->insert([
            'title' => 'Pet Name',
            'description' => 'What is your pet name?',
        ]);
        DB::table('securityques')->insert([
            'title' => 'Car Color',
            'description' => 'What was the color of your first car?',
        ]);
        DB::table('securityques')->insert([
            'title' => 'Birth City',
            'description' => 'In what city were you born?',
        ]);
        DB::table('securityques')->insert([
            'title' => 'Childhood Hero',
            'description' => 'Who was your childhood hero?',
        ]);
        DB::table('securityques')->insert([
            'title' => 'Card Last Digit',
            'description' => 'What are the last 5 digits of your credit card?',
        ]);
        DB::table('securityques')->insert([
            'title' => 'First Job City',
            'description' => 'In what city or town was your first job?',
        ]);
        DB::table('securityques')->insert([
            'title' => 'Favorite Sport',
            'description' => 'What is your favorite sport?',
        ]);
        DB::table('securityques')->insert([
            'title' => 'Favorite Teacher',
            'description' => 'What was the last name of your favorite teacher?',
        ]);
        DB::table('securityques')->insert([
            'title' => 'Favorite Teacher',
            'description' => 'What is your favorite team?',
        ]);
    }
}
