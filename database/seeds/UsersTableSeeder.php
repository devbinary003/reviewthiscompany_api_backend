<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'admin',
            'email' => 'admin@email.com',
            'password' => Hash::make('admin@123'),
            'firstname' => 'Admin',
            'lastname' => 'Admin',
            'fullname' => 'Admin',
            'phone' => '9876543210',
            'address' => 'Phase 8',
            'city' => 'Mohali',
            'state' => 'Punjab',
            'country' => 'India',
            'zipcode' => '160071',

        ]);
    }
}
