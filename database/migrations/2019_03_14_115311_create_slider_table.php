<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSliderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slider', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('slider_order_no')->nullable();
            $table->string('image', 191)->nullable();
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('button_text', 191)->nullable();
            $table->string('button_url', 191)->nullable();
            $table->enum('isActive',['1', '0'])->default('1')->comment="1=Active,0=Deactive";
            $table->enum('isDeleted',['1', '0'])->default('0')->comment="1=Deleted,0=Not Deleted";
            $table->string('created_by', 191)->nullable();
            $table->string('updated_by', 191)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slider');
    }
}
