<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToBusinesssetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('businesssetting', function (Blueprint $table) {
            //
            $table->string('google_map_url')->after('stripe_live_sec_key')->nullable();
            $table->string('ex1')->after('google_map_url')->nullable();
            $table->string('ex2')->after('ex1')->nullable();
            $table->string('ex3')->after('ex2')->nullable();
            $table->string('ex4')->after('ex3')->nullable();
            $table->string('ex5')->after('ex4')->nullable();
            $table->string('ex6')->after('ex5')->nullable();
            $table->string('ex7')->after('ex6')->nullable();
            $table->string('ex8')->after('ex7')->nullable();
            $table->string('ex9')->after('ex8')->nullable();
            $table->string('ex10')->after('ex9')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('businesssetting', function (Blueprint $table) {
            //
        });
    }
}
