<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location_feedback', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('location_id');
            $table->integer('user_id');
            $table->text('comment');
            $table->integer('rating');
            $table->enum('status',['1', '0', '2'])->default('2')->comment="1=Approve,0=Decline,2=Pending";
            $table->enum('deleted',['1', '0'])->default('0')->comment="1=Deleted,0=Not Deleted";
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('location_feedback');
    }
}
