<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location', function (Blueprint $table) {
            $table->increments('id');
            $table->string('locationname', 191)->nullable();
            $table->text('locationdescription')->nullable();
            $table->string('locationimage', 191)->nullable();
            $table->integer('locationtypeid')->nullable();
            $table->string('locationgoogleurl', 191)->nullable();
            $table->string('button_text', 191)->nullable();
            $table->string('button_url', 191)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('location');
    }
}
