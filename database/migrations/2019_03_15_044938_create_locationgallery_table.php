<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationgalleryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locationgallery', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('locationid')->nullable();
            $table->text('image')->nullable();
            $table->integer('order_no')->nullable();
            $table->string('button_text', 191)->nullable();
            $table->string('button_url', 191)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locationgallery');
    }
}
