<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 191)->unique()->nullable();
            $table->string('email', 191)->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password', 191)->nullable();
            $table->string('firstname', 191)->nullable();
            $table->string('lastname', 191)->nullable();
            $table->string('fullname', 191)->nullable();
            $table->string('title', 191)->nullable();
            $table->text('description')->nullable();
            $table->string('phone', 191)->nullable();
            $table->text('address', 191)->nullable();
            $table->string('city', 191)->nullable();
            $table->string('state', 191)->nullable();
            $table->string('country', 191)->nullable();
            $table->string('zipcode', 191)->nullable();
            $table->string('lat', 191)->nullable();
            $table->string('long', 191)->nullable();
            $table->text('profilepic')->nullable();
            $table->text('security_ques')->nullable();
            $table->text('security_ans')->nullable();
            $table->string('role', 191)->nullable();
            $table->string('gender', 191)->nullable();
            $table->date('dob', 191)->nullable();
            $table->text('device')->nullable();
            $table->text('browser')->nullable();
            $table->string('ipaddress', 191)->nullable();
            $table->string('active_code', 191)->nullable();
            $table->enum('status',['0', '1'])->default('1')->comment="1=Active,0=Inactive";
            $table->enum('isdeleted',['0', '1'])->default('0')->comment="1=Deleted,0=Not Deleted";
            $table->enum('isonline',['0', '1'])->default('0')->comment="1=Online,0=Offline";
            $table->enum('isapproved',['0', '1', '2'])->default('1')->comment="1=Approved,0=Not Approved,2=Declined";
            $table->integer('logins')->default('0');
            $table->timestamp('last_login')->nullable();
            $table->rememberToken();
            $table->string('created_by', 191)->nullable();
            $table->string('updated_by', 191)->nullable();
            $table->timestamps();
            $table->text('extra1')->nullable();
            $table->text('extra2')->nullable();
            $table->text('extra3')->nullable();
            $table->text('extra4')->nullable();
            $table->text('extra5')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
