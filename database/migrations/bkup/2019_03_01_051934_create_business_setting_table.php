<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesssetting', function (Blueprint $table) {
            $table->increments('id');
            $table->string('business_name', 191)->nullable();
            $table->string('business_address', 191)->nullable();
            $table->string('email', 191)->nullable();
            $table->string('phone', 191)->nullable();
            $table->string('website', 191)->nullable();
            $table->string('logo', 191)->nullable();
            $table->string('facebook_link', 191)->nullable();
            $table->string('twitter_link', 191)->nullable();
            $table->string('instagram_link', 191)->nullable();
            $table->string('linkedin_link', 191)->nullable();
            $table->string('google_link', 191)->nullable();
            $table->string('default_currency', 191)->default('USD')->nullable();
            $table->string('language', 191)->default('English')->nullable();
            $table->string('payment_mode', 191)->nullable();
            $table->string('paypal_test_email', 191)->nullable();
            $table->string('paypal_live_email', 191)->nullable();
            $table->string('stripe_test_pub_key', 191)->nullable();
            $table->string('stripe_test_sec_key', 191)->nullable();
            $table->string('stripe_live_pub_key', 191)->nullable();
            $table->string('stripe_live_sec_key', 191)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_setting');
    }
}
