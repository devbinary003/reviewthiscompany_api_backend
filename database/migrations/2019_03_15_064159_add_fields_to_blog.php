<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToBlog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blog', function (Blueprint $table) {
            //
            $table->enum('status',['1', '0'])->default('1')
            ->after('button_url')
            ->comment="1=Active,0=Deactive";
            $table->enum('deleted',['1', '0'])->default('0')
            ->after('status')
            ->comment="1=Deleted,0=Not Deleted";
            $table->integer('created_by')->after('deleted')->nullable();
            $table->integer('updated_by')->after('created_by')->nullable();
            $table->timestamp('deleted_date')->after('updated_by')->nullable();
            $table->integer('deleted_by')->after('deleted_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blog', function (Blueprint $table) {
            //
        });
    }
}
