<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ModalController extends Controller
{
    //
    public function modal()
    {
    	return view('modele.modal');
    }
}
