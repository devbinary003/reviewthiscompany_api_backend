<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LockscreenController extends Controller
{
    //
    public function lockscreen() {
    	return view('auth.lockscreen');
    }
}
