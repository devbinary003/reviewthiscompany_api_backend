<?php
namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdvanceFormController extends Controller
{
    //
    public function advance_form()
    {
    	return view('forms.advance');
    }
}
