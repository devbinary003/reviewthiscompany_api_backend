<?php
namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SimpleTableController extends Controller
{
    //
    public function simpleTable(){
    	return view('tables.simple');
    }
}
