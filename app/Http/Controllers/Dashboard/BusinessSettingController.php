<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BusinessSettingController extends Controller
{
    //
    public function index()
    {
    	return view('user.business-setting');
    }
}
