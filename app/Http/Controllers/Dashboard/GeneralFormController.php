<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GeneralFormController extends Controller
{
    //
    public function general_form()
    {
    	return view('forms.general');
    }
}
