<?php
namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MailboxController extends Controller
{
    //
    public function index() {
    	return view('mailbox.mail');
    }

    public function compose_message() {
    	return view('mailbox.compose_message');
    }

	public function read_mail()
	{
		return view('mailbox.read_mail');
	}
}
