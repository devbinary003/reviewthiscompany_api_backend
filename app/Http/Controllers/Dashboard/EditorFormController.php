<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EditorFormController extends Controller
{
    //

    public function editor_form(){
    	return view('forms.editor');
    }
}
