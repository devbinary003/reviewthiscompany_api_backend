<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Businessreview;
use App\Models\Businesslocations;
use Illuminate\Support\Facades\Auth;
use App\Mail\SendMailSatisfiedReview;
use App\Mail\SendMailUnSatisfiedReview;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon; 
use JWTFactory;
use JWTAuth;
use Validator;
use Config;
use Log;
use Event;
use Mail;
use App\Events\UserRegistered;
use DB;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Classes\ErrorsClass;
use Image;

class BusinessreviewController extends Controller
{  

  public function getbusinessdetails(Request $request, $name){
        try {

          $chkBisinessName = Businesslocations::where('business_review_link', '=', trim($name))->where('status', '=', '1')->where('isdeleted', '=', '0')->count();

          if($chkBisinessName > 0) {

            $business_details = Businesslocations::where('business_review_link', '=', trim($name))->where('status', '=', '1')->where('isdeleted', '=', '0')->first();

            $user_details = User::where('id', '=', $business_details->user_id)->where('status', '=', '1')->where('isdeleted', '=', '0')->first();

            return response()->json(['status'=>true,'message'=>'Business details','error'=>'','data'=>$business_details, 'user'=>$user_details], 200);

          } else {
            return response()->json(['status'=>false,'message'=>'Business name not found','error'=>'','data'=>''], 400);
          }

        } catch(\Illuminate\Database\QueryException $e) {
                $errorClass = new ErrorsClass();
                $errors = $errorClass->saveErrors($e);
                return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
        } catch(\Exception $e) {
                $errorClass = new ErrorsClass();
                $errors = $errorClass->saveErrors($e);
                return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
        }
    }

  public function checkReviewerBusinessReview(Request $request) {
    try{

      $userid = trim($request->userid);
      $reviewer_email = trim($request->reviewer_email);
      $business_slug = trim($request->business_slug);
      $reviewer_rating = trim($request->reviewer_rating);

      $businessInfo = Businesslocations::where('business_review_link', '=', $business_slug)->where('status', '=', '1')->where('isdeleted', '=', '0')->first();

      if($businessInfo->client_satisfaction == 0){
          $clientSatisfactionVal = '5';
      } else {
          $clientSatisfactionVal = '4';
      }

      $chkReviewCount = Businessreview::where('reviewer_email', '=', $reviewer_email)
        ->where('business_slug', '=', $business_slug)
        ->where('status', '=', '1')
        ->where('is_deleted', '=', '0')
        ->count();

      if($chkReviewCount > 0) {

        $reviewInfo = Businessreview::where('reviewer_email', '=', $reviewer_email)
        ->where('business_slug', '=', $business_slug)
        ->where('status', '=', '1')
        ->where('is_deleted', '=', '0')
        ->orderby('id','DESC')
        ->first();

        if($reviewInfo->reviewer_rating >= $clientSatisfactionVal) {
         $rtnMsg = 'You have already given ' .$reviewInfo->reviewer_rating. ' star rating so you are not able to rate again';
         return response()->json(['status'=>false,'message'=>$rtnMsg,'error'=>'','data'=>''], 200); 
        } else {
          return response()->json(['status'=>true,'message'=>'Submit review3','error'=>'','data'=>''], 200); 
        }

      } else {
        return response()->json(['status'=>true,'message'=>'Submit review1','error'=>'','data'=>''], 200);
      } 
      
    } catch(\Illuminate\Database\QueryException $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }
  }  

  public function create(Request $request){
    
    try{

      $userid = trim($request->business_user_id);
      $reviewer_email = trim($request->email);
      $reviewer_name = trim($request->name);
      $business_slug = trim($request->business_name);
      $reviewer_rating = trim($request->reviewer_rating);
      $reviewer_description = trim($request->reviewer_description);
      $businessInfo = Businesslocations::where('business_review_link', '=', $business_slug)->where('status', '=', '1')->where('isdeleted', '=', '0')->first();

      $userInfo = User::where('id', '=', $businessInfo->user_id)->where('status', '=', '1')->where('isdeleted', '=', '0')->first();

      if($businessInfo->client_satisfaction == 0){
          $clientSatisfactionVal = '5';
      } else {
          $clientSatisfactionVal = '4';
      }

      $chkReviewCount = Businessreview::where('reviewer_email', '=', $reviewer_email)
        ->where('business_slug', '=', $business_slug)
        ->where('status', '=', '1')
        ->where('is_deleted', '=', '0')
        ->count();

        if($chkReviewCount > 0) {

        $reviewInfo = Businessreview::where('reviewer_email', '=', $reviewer_email)
        ->where('business_slug', '=', $business_slug)
        ->where('status', '=', '1')
        ->where('is_deleted', '=', '0')
        ->orderby('id','DESC')
        ->first();

        if($reviewInfo->reviewer_rating >= $clientSatisfactionVal) {

         $rtnMsg = 'You have already given ' .$reviewInfo->reviewer_rating. ' star rating so you are not able to rate again';
         return response()->json(['status'=>false,'message'=>$rtnMsg,'error'=>'','data'=>''], 200); 

        } else {

        $Input =  [];
        $Input['reviewer_name'] = trim($request->name);
        $Input['reviewer_email'] = trim($request->email);
        $Input['reviewer_rating'] = trim($request->reviewer_rating);
        $Input['reviewer_description'] = trim($request->reviewer_description);
        $Input['business_user_id'] = trim($request->business_user_id);
        $Input['business_place_id'] = trim($request->business_place_id);
        $Input['business_slug'] = trim($request->business_name);
        $Input['status'] = '1';
        $Input['is_deleted'] = '0';
        $submitreview = Businessreview::create($Input);
        $review_last_insert_id = $submitreview->id;
        if($review_last_insert_id){

          if($clientSatisfactionVal == 5 && $reviewer_rating >= 5) {
            $sendmail = Mail::to($reviewer_email)->send(new SendMailSatisfiedReview($userInfo->sc_emsg, $userInfo->sc_ebtnclr, $userInfo->sc_enote, $userInfo->email, $userInfo->fullname, $businessInfo->business_name, $businessInfo->business_address, $businessInfo->business_page_id, $reviewer_name, $review_last_insert_id, $reviewer_description ));
          } elseif($clientSatisfactionVal == 4 && $reviewer_rating >= 4) {
            $sendmail = Mail::to($reviewer_email)->send(new SendMailSatisfiedReview($userInfo->sc_emsg, $userInfo->sc_ebtnclr, $userInfo->sc_enote, $userInfo->email, $userInfo->fullname, $businessInfo->business_name, $businessInfo->business_address, $businessInfo->business_page_id, $reviewer_name, $review_last_insert_id, $reviewer_description ));
          } else {
            $sendmail = Mail::to($reviewer_email)->send(new SendMailUnSatisfiedReview($userInfo->unsc_emsg, $userInfo->sc_ebtnclr, $userInfo->email, $userInfo->fullname, $businessInfo->business_name, $businessInfo->business_address, $businessInfo->business_page_id, $reviewer_name, $review_last_insert_id, $reviewer_description ));
          }

          return response()->json(['status'=>true,'message'=>'Thank you! your review submitted successfully.','error'=>'','data'=>$submitreview], 200); 
          } else {
            return response()->json(['status'=>false,'message'=>'Sorry fail to submit your review. Please try again.','error'=>'','data'=>''], 200);
          }
        }

      } else {

        $Input =  [];
        $Input['reviewer_name'] = trim($request->name);
        $Input['reviewer_email'] = trim($request->email);
        $Input['reviewer_rating'] = trim($request->reviewer_rating);
        $Input['reviewer_description'] = trim($request->reviewer_description);
        $Input['business_user_id'] = trim($request->business_user_id);
        $Input['business_place_id'] = trim($request->business_place_id);
        $Input['business_slug'] = trim($request->business_name);
        $Input['status'] = '1';
        $Input['is_deleted'] = '0';
        $submitreview = Businessreview::create($Input);
        $review_last_insert_id = $submitreview->id;
        if($review_last_insert_id){

          if($clientSatisfactionVal == 5 && $reviewer_rating >= 5) {
            $sendmail = Mail::to($reviewer_email)->send(new SendMailSatisfiedReview($userInfo->sc_emsg, $userInfo->sc_ebtnclr, $userInfo->sc_enote, $userInfo->email, $userInfo->fullname, $businessInfo->business_name, $businessInfo->business_address, $businessInfo->business_page_id, $reviewer_name, $review_last_insert_id, $reviewer_description ));
          } elseif($clientSatisfactionVal == 4 && $reviewer_rating >= 4) {
            $sendmail = Mail::to($reviewer_email)->send(new SendMailSatisfiedReview($userInfo->sc_emsg, $userInfo->sc_ebtnclr, $userInfo->sc_enote, $userInfo->email, $userInfo->fullname, $businessInfo->business_name, $businessInfo->business_address, $businessInfo->business_page_id, $reviewer_name, $review_last_insert_id, $reviewer_description ));
          } else {
            $sendmail = Mail::to($reviewer_email)->send(new SendMailUnSatisfiedReview($userInfo->unsc_emsg, $userInfo->sc_ebtnclr, $userInfo->email, $userInfo->fullname, $businessInfo->business_name, $businessInfo->business_address, $businessInfo->business_page_id, $reviewer_name, $review_last_insert_id, $reviewer_description ));
          }

          return response()->json(['status'=>true,'message'=>'Thank you! your review submitted successfully.','error'=>'','data'=>$submitreview], 200); 
          } else {
            return response()->json(['status'=>false,'message'=>'Sorry fail to submit your review. Please try again.','error'=>'','data'=>''], 200);
          }

        } 

        } catch(\Illuminate\Database\QueryException $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
            return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
            return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
        }
        
  }
  
  public function submitReviewCallbk(Request $request){
        try {
          if(trim($request->key)!='') {
            $id = base64_decode(trim($request->key));
            $reviewInfo = Businessreview::where('id', '=', $id)->where('status', '=', '1')->where('is_deleted', '=', '0')->first();
            if($reviewInfo) {
              $Input =  [];
              $Input['is_complete'] = '1';
              $Input['updated_at'] = date('Y-m-d h:i:s');
              Businessreview::where('id', $id)->update($Input);
              $redirectUrl = "https://search.google.com/local/writereview?placeid=".$reviewInfo->business_place_id;
              return redirect($redirectUrl);
              exit;
            } else {
              return redirect(env('RVG_COMP_URL'));
              exit;
            }
          } else {
            return redirect(env('RVG_COMP_URL'));
              exit;
          }
          exit;
        } catch(\Illuminate\Database\QueryException $e) {
                $errorClass = new ErrorsClass();
                $errors = $errorClass->saveErrors($e);
                return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
        } catch(\Exception $e) {
                $errorClass = new ErrorsClass();
                $errors = $errorClass->saveErrors($e);
                return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
        }
    }

  /*public function create(Request $request){
    
    try{
        $rules = [
             'username' => 'unique:users',
             'email' => 'unique:users',
         ];

         $messages = [
             'username.unique'   =>  'That user name is already in use.',
             'email.unique'   =>  'That email address is already in use.',
         ];

         $validator = Validator::make($request->all(), $rules, $messages);
         if ($validator->fails()) 
         { 
             return response()->json(['status'=>false,'message'=>'Email address or Name already in use','error'=>$validator->errors(),'data'=>'']);
         }

        $UserInput = [];
        $UserInput['username'] = trim($request->name);
        $UserInput['email'] = trim($request->email);
        $UserInput['role'] = '3';
        $UserInput['browser'] = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null;
        $saveuserdata = User::create($UserInput);

        $last_insert_id = $saveuserdata->id;

        if($last_insert_id){

        $Input =  [];

        $Input['reviewer_id'] = $last_insert_id;
        $Input['reviewer_name'] = trim($request->name);
        $Input['reviewer_email'] = trim($request->email);
        $Input['reviewer_rating'] = trim($request->reviewer_rating);
        $Input['reviewer_description'] = trim($request->reviewer_description);

        $Input['business_user_id'] = trim($request->business_user_id);
        $Input['business_place_id'] = trim($request->business_place_id);
        $Input['business_slug'] = trim($request->business_name);
        
        $Input['status'] = '1';
        $Input['is_deleted'] = '0';
        $Input['created_by'] = $last_insert_id;

        $submitreview = Businessreview::create($Input);

        $review_last_insert_id = $submitreview->id;

        if($review_last_insert_id){
          
           $reviewer_rating = trim($request->reviewer_rating);
           $reviewer_name = trim($request->name);
           $reviewer_email = trim($request->email);

           $businessuserdata = User::find(trim($request->business_user_id));

           $business_user_id = $businessuserdata->id;
           $business_user_email = $businessuserdata->email;
           $business_user_name = $businessuserdata->fullname;

           $business_name = $businessuserdata->business_name;
           $business_address = $businessuserdata->business_address;
           $business_page_id = $businessuserdata->business_page_id;

           $client_satisfaction = $businessuserdata->client_satisfaction;
           $sc_emsg = $businessuserdata->sc_emsg;
           $sc_ebtnclr = $businessuserdata->sc_ebtnclr;
           $sc_enote = $businessuserdata->sc_enote;
           $unsc_emsg = $businessuserdata->unsc_emsg;

           if($client_satisfaction == 0 && $reviewer_rating > 4 ) {

            $sendmail = Mail::to($reviewer_email)->send(new SendMailSatisfiedReview($sc_emsg, $sc_ebtnclr, $sc_enote, $business_user_email, $business_user_name, $business_name, $business_address, $business_page_id, $reviewer_name ));

           } elseif($client_satisfaction == 1 && $reviewer_rating > 3) {

            $sendmail = Mail::to($reviewer_email)->send(new SendMailSatisfiedReview($sc_emsg, $sc_ebtnclr, $sc_enote, $business_user_email, $business_user_name, $business_name, $business_address, $business_page_id, $reviewer_name ));

           } else {

            $sendmail = Mail::to($reviewer_email)->send(new SendMailUnSatisfiedReview($unsc_emsg, $sc_ebtnclr, $business_user_email, $business_user_name, $business_name, $business_address, $business_page_id, $reviewer_name));
           }
        
          return response()->json(['status'=>true,'message'=>'Thank you! your review submitted successfully.','error'=>'','data'=>$submitreview], 200); 
       
          } else {
            return response()->json(['status'=>false,'message'=>'Sorry fail to submit your review. Please try again.','error'=>'','data'=>''], 200);
          }
        
        } else {
            return response()->json(['status'=>false,'message'=>'Sorry fail to submit your review. Please try again.','error'=>'','data'=>''], 200);
        }

        } catch(\Illuminate\Database\QueryException $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
            return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
            return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
        }
        
  }*/

    

    public function getallReviewsLists(Request $request) {
      try{
          $searchdata = $request->searchdata;
          if($searchdata!=''){
          $business_owner_id = $request->userid;
          $userdata = User::where('id',$business_owner_id)->first();
          $client_satisfaction = $userdata->client_satisfaction;
          if($client_satisfaction == 1){
             $clientsecondvalue = '3';
          } else {
             $clientsecondvalue = '4';
          }
          /*$client_satisfactionarr = explode('-', $client_satisfaction);
          $clientsecondvalue = $client_satisfactionarr[1]; */
          if ($searchdata == 1){
            $business_owner_id = $request->userid;
            $reviewlist = Businessreview::where('business_user_id',$business_owner_id)->where('status', '=', '1')->where('is_deleted', '=', '0')->where('reviewer_rating', '>', $clientsecondvalue)->orderby('id','DESC')->paginate(Config::get('constant.pagination'));
            return response()->json(['status'=>true,'message'=>'Review Listing','error'=>'','data'=>$reviewlist], 200);
          }
          elseif($searchdata == 2){
            $business_owner_id = $request->userid;
            $reviewlist = Businessreview::where('business_user_id',$business_owner_id)->where('status', '=', '1')->where('is_deleted', '=', '0')->where('reviewer_rating', '<=', $clientsecondvalue)->orderby('id','DESC')->paginate(Config::get('constant.pagination'));
            return response()->json(['status'=>true,'message'=>'Review Listing','error'=>'','data'=>$reviewlist], 200);
          } else {
            $business_owner_id = $request->userid;
            $reviewlist = Businessreview::where('business_user_id',$business_owner_id)->where('status', '=', '1')->where('is_deleted', '=', '0')->orderby('id','DESC')->paginate(Config::get('constant.pagination'));
            return response()->json(['status'=>true,'message'=>'Review Listing','error'=>'','data'=>$reviewlist], 200);
          }
        } else {
            $business_owner_id = $request->userid;
            $reviewlist = Businessreview::where('business_user_id',$business_owner_id)->where('status', '=', '1')->where('is_deleted', '=', '0')->orderby('id','DESC')->paginate(Config::get('constant.pagination'));
            return response()->json(['status'=>true,'message'=>'Review Listing','error'=>'','data'=>$reviewlist], 200);
        }
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
    }

    public function getallUserReviewsLists(Request $request) {
      try{
          $user_id = $request->userid;
          $reviewlist = DB::table('business_reviews')
            ->join('users', 'business_reviews.reviewer_id', '=', 'users.id')
            ->where('business_reviews.reviewer_id', $user_id)
            ->where('business_reviews.status', '=', '1')
            ->where('business_reviews.is_deleted', '=', '0')
            ->select('users.*', 'business_reviews.*')
            ->paginate(Config::get('constant.pagination'));
          return response()->json(['status'=>true,'message'=>'Review Listing','error'=>'','data'=>$reviewlist], 200);
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
    }

    public function getUserReviewsLists(Request $request) {
      try{
          $reviewlist = DB::table('business_reviews')
            ->join('users', 'business_reviews.reviewer_id', '=', 'users.id')
            ->where('users.role','=','3')
            ->where('business_reviews.status', '=', '1')
            ->where('business_reviews.is_deleted', '=', '0')
            ->select('users.*', 'business_reviews.*')
            ->paginate(Config::get('constant.pagination'));
          return response()->json(['status'=>true,'message'=>'Review Listing','error'=>'','data'=>$reviewlist], 200);
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
    }

    public function getSingleUserReviewsLists(Request $request) {
      try{
          $user_id = $request->userid;
          $reviewlist = DB::table('business_reviews')
            ->join('users', 'business_reviews.reviewer_id', '=', 'users.id')
            ->where('business_reviews.business_user_id', $user_id)
            ->where('business_reviews.status', '=', '1')
            ->where('business_reviews.is_deleted', '=', '0')
            ->select('users.*', 'business_reviews.*')
            ->paginate(Config::get('constant.pagination'));
          return response()->json(['status'=>true,'message'=>'Review Listing','error'=>'','data'=>$reviewlist], 200);
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
    }

    public function ReviewBusinessUserCount(Request $request) {
    try{
      $userid = $request->userid;
      $user = Businessreview::where('reviewer_id', $userid)->where('status', '=', '1')->where('is_deleted', '=', '0')->first();
      $usercount = Businessreview::where('reviewer_id', $user->reviewer_id)->where('business_user_id', $user->business_user_id)->where('status', '=', '1')->where('is_deleted', '=', '0')->groupBy('business_user_id')->get();
      if ($usercount) {
        return response()->json(['status'=>true,'message'=>'Business Owner Users count','error'=>'','data'=>$usercount], 200);
      } else {
        return response()->json(['status'=>false,'message'=>'No data','error'=>'','data'=>''], 402);
      }
    } catch(\Illuminate\Database\QueryException $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }
  }
    




}