<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Businessreview extends Model
{

	protected $table = 'business_reviews';

    protected $fillable = [
        'id', 'reviewer_id', 'reviewer_name', 'reviewer_email', 'reviewer_rating', 'reviewer_description', 'business_user_id', 'business_place_id', 'business_slug', 'status', 'is_deleted', 'is_complete', 'created_by', 'updated_by', 'created_at', 'updated_at'
    ];
    
}