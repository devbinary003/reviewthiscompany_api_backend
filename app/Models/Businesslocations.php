<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Businesslocations extends Model
{

	protected $table = 'businesslocations';

    protected $fillable = [
        'id', 'user_id', 'find_business_location', 'business_name', 'business_address', 'business_rating', 'business_page_id', 'lat', 'lng', 'phone', 'business_logo', 'business_review_link', 'facebook_link', 'twitter_link', 'linkedin_link', 'instagram_link', 'client_satisfaction', 'status', 'isdeleted', 'created_by', 'updated_by',
    ];
    
}