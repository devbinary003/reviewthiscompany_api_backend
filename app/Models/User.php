<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Http\Resources\UserResource;
use Laravel\Cashier\Billable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'username', 'email', 'password', 'hdpwd', 'firstname', 'lastname', 'fullname', 'phone', 'profilepic', 'role', 'device', 'browser', 'ipaddress', 'active_code', 'isonline', 'llhid', 'number_of_locations', 'item_qnty', 'total_sum', 'sc_emsg', 'sc_ebtnclr', 'sc_enote', 'unsc_emsg', 'customer_id', 'subscription_id', 'plan_id', 'amount', 'subscription_status', 'current_period_start', 'current_period_end', 'start_date', 'card_number', 'card_exp_month', 'exp_year', 'name_on_card', 'reg_step_1', 'reg_step_2', 'reg_step_3', 'reg_step_4', 'isprofilecomplete', 'status', 'isdeleted', 'isapproved', 'isactivationcomplete', 'logins', 'last_login', 'created_by', 'updated_by',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function message()
    {
        return $this->hasMany('App\Models\Message', 'sender_id', 'id');
    }
}
