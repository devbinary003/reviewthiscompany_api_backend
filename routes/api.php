 <?php

use Illuminate\Http\Request;

Route::group(['middleware' => ['api','cors'], 'prefix' => 'review'], function () {

  Route::get('getbusinessdetails/{name}', 'Api\BusinessreviewController@getbusinessdetails');
  Route::get('checkreviewer', 'Api\BusinessreviewController@checkReviewerBusinessReview');
  Route::post('reviewsubmit', 'Api\BusinessreviewController@create');
  Route::get('srcallbk', 'Api\BusinessreviewController@submitReviewCallbk');
  
  Route::get('getreviewlist', 'Api\BusinessreviewController@getallReviewsLists');
  Route::get('getuserreviewlist', 'Api\BusinessreviewController@getallUserReviewsLists');
  Route::get('getuserreviewlists', 'Api\BusinessreviewController@getUserReviewsLists');
  Route::get('getsingleuserreviewslists', 'Api\BusinessreviewController@getSingleUserReviewsLists');
  Route::get('reviewbusinessusercount', 'Api\BusinessreviewController@ReviewBusinessUserCount');
});

