<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/dashboard1','Dashboard\Dashboard1Controller@index');
Route::get('/dashboard2','Dashboard\Dashboard2Controller@index');
Route::get('/profile','Dashboard\ProfileController@profile_view');
Route::get('/business-setting','Dashboard\BusinessSettingController@index');

Route::get('/generalform', 'Dashboard\GeneralFormController@general_form');

Route::get('/advanceform','Dashboard\AdvanceFormController@advance_form' );

Route::get('/editorform','Dashboard\EditorFormController@editor_form');

Route::get('/simpletable','Dashboard\SimpleTableController@simpleTable');

Route::get('/post-list','Dashboard\PostController@getList');
Route::get('/post-edit','Dashboard\PostController@postEdit');
Route::get('/post-view','Dashboard\PostController@getSinglePost');

Route::get('/user-list','Dashboard\UserController@getUserList');
Route::get('/user-edit','Dashboard\UserController@EditUserList');
Route::get('/user-view','Dashboard\UserController@getSingleUser');



Route::get('/register2','Dashboard\RegisterController@register1' );

Route::get('/register3','Dashboard\RegisterController@register1');

Route::get('/mail','Dashboard\MailboxController@index');

Route::get('/compose_message', 'Dashboard\MailboxController@compose_message');

Route::get('/read_mail', 'Dashboard\MailboxController@read_mail');

Route::get('/chart','Dashboard\ChartController@chart');

Route::get('/modal', 'Dashboard\ModalController@modal');

Route::get('/setting', 'Dashboard\SettingController@setting');

Route::get('/lockscreen', 'Dashboard\LockscreenController@lockscreen');
