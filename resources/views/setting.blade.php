@extends('layouts.default')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 960px;">

    <!-- Main content -->
    <section class="content setting-back">

      <div class="row">
        
        <div class="col-sm-2">
            <!-- required for floating -->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
              <h6><i class="fa fa-wrench"></i> App Settings</h6>
                <li class="active"><a href="#home" data-toggle="tab" class="ger">General</a></li>
                <li><a href="#profile" data-toggle="tab" class="ger">Company</a></li>
                <li><a href="#messages" data-toggle="tab" class="ger">Paymet Mathed</a></li>
                <li><a href="#settings" data-toggle="tab" class="ger">Roles</a></li>
            </ul>
        </div>
        <div class="col-sm-10">
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane contab active" id="home">
                  <h6>Gernal Settings</h6>
                  <hr>
                  <div class="row form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label labelweight">Image </label>
                    <div class="col-sm-9">
                      <img src="./public/images/default-50x50.gif" alt="" class="profile-image">
                    </div>
                  </div>
                  <div class="row form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label labelweight">Show logo in Signin page </label>
                    <div class="col-sm-9">
                      <select class="form-control custom_width">
                        <option>No</option>
                        <option>Yes</option>
                      </select>
                    </div>
                  </div>
                  <div class="row form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label labelweight">Show background image in Signin page </label>
                    <div class="col-sm-9">
                      <select class="form-control custom_width">
                        <option>No</option>
                        <option>Yes</option>
                      </select>
                    </div>
                  </div>
                  <div class="row form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label labelweight">Signin page background</label>
                    <div class="col-sm-9">
                      <img src="./public/images/default-50x50.gif" alt="" class="profile-image">
                    </div>
                  </div>
                  <div class="row form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label labelweight">App Title</label>
                    <div class="col-sm-9">
                      <input class="form-control" type="text" placeholder="Default input">
                    </div>
                  </div>
                  <div class="row form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label labelweight">Language</label>
                    <div class="col-sm-9">
                      <select class="form-control custom_width">
                        <option>English</option>
                        <option>US English</option>
                      </select>
                    </div>
                  </div>
                  <div class="row form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label labelweight">Timezone</label>
                    <div class="col-sm-9">
                      <select class="form-control custom_width">
                        <option>UTC</option>
                        <option>UTC</option>
                      </select>
                    </div>
                  </div>
                  <div class="row form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label labelweight">Date Formate</label>
                    <div class="col-sm-9">
                      <select class="form-control custom_width">
                        <option>Y-M-D</option>
                        <option>Y-M-D</option>
                      </select>
                    </div>
                  </div>
                  <div class="row form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label labelweight">Time Formate</label>
                    <div class="col-sm-9">
                      <select class="form-control custom_width">
                        <option>12 am</option>
                        <option>12 am</option>
                      </select>
                    </div>
                  </div>
                  <div class="row form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label labelweight">First day of week</label>
                    <div class="col-sm-9">
                      <select class="form-control custom_width">
                        <option>Sunday</option>
                        <option>Monday</option>
                        <option>Tusday</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="profile">Profile Tab.</div>
                <div class="tab-pane" id="messages">Messages Tab.</div>
                <div class="tab-pane" id="settings">
                  <div class="row">
                    <div class="col-sm-5">
                      <div class="taberole">
                        <h6>Roles <a class="add_role"  data-toggle="modal" data-target="#addmodel"><i class="fa fa-plus-square"></i> Add role</a></h6>
                        <ul class="nav nav-tabs">
                          <li href="#business" data-toggle="tab" class="ger active" aria-expanded="false">Business Developer
                            <ul>
                              <li><a href="" class="rolei"><i class="fa fa-check"></i></a></li>
                              <li><a href="" class="rolei"><i class="fa fa-edit"></i></a></li>
                              <li><a href="" class="rolei"><i class="fa fa-times"></i></a></li>
                            </ul>
                          </li>
                          <li href="#client" data-toggle="tab" class="ger" aria-expanded="false">Client
                            <ul>
                              <li><a href="" class="rolei"><i class="fa fa-check"></i></a></li>
                              <li><a href="" class="rolei"><i class="fa fa-edit"></i></a></li>
                              <li><a href="" class="rolei"><i class="fa fa-times"></i></a></li>
                            </ul>
                          </li>
                          <li href="#graphic" data-toggle="tab" class="ger" aria-expanded="false">Graphic Designer
                            <ul>
                              <li><a href="" class="rolei"><i class="fa fa-check"></i></a></li>
                              <li><a href="" class="rolei"><i class="fa fa-edit"></i></a></li>
                              <li><a href="" class="rolei"><i class="fa fa-times"></i></a></li>
                            </ul>
                          </li>
                          <li href="#UI" data-toggle="tab" class="ger" aria-expanded="false">UI/UX Designer
                            <ul>
                              <li><a href="" class="rolei"><i class="fa fa-check"></i></a></li>
                              <li><a href="" class="rolei"><i class="fa fa-edit"></i></a></li>
                              <li><a href="" class="rolei"><i class="fa fa-times"></i></a></li>
                            </ul>
                          </li>
                          <li href="#web" data-toggle="tab" class="ger" aria-expanded="false">Web Designer
                            <ul>
                              <li><a href="" class="rolei"><i class="fa fa-check"></i></a></li>
                              <li><a href="" class="rolei"><i class="fa fa-edit"></i></a></li>
                              <li><a href="" class="rolei"><i class="fa fa-times"></i></a></li>
                            </ul>
                          </li>
                          <li href="#seo" data-toggle="tab" class="ger" aria-expanded="false">Web Seo
                            <ul>
                              <li><a href="" class="rolei"><i class="fa fa-check"></i></a></li>
                              <li><a href="" class="rolei"><i class="fa fa-edit"></i></a></li>
                              <li><a href="" class="rolei"><i class="fa fa-times"></i></a></li>
                            </ul>
                          </li>
                          <li href="#quality" data-toggle="tab" class="ger" aria-expanded="false">Quality Assurance
                            <ul>
                              <li><a href="" class="rolei"><i class="fa fa-check"></i></a></li>
                              <li><a href="" class="rolei"><i class="fa fa-edit"></i></a></li>
                              <li><a href="" class="rolei"><i class="fa fa-times"></i></a></li>
                            </ul>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-sm-7">
                      <div class="tab-content">
                        <div class="roledetail tab-pane active" id="business">
                          <h6>Permissions: Business Developer</h6>
                          <hr>
                          <div class="dropdown normalmenu">
                            <h5 class="dropdown-toggle" type="button" data-toggle="dropdown">Set project permissions :<span class="caret"></span></h5>
                            <ul class="dropdown-menu">
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                              <li><a href="#"></a></li>
                            </ul>
                          </div>
                
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Can Add
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Can Edit
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Can View
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Can Delete
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="dropdown normalmenu">
                            <h5 class="dropdown-toggle" type="button" data-toggle="dropdown">Set project permissions :<span class="caret"></span></h5>
                            <ul class="dropdown-menu">
                              <li><a href="#">Can Add</a></li>
                              <li><a href="#">Can Edit</a></li>
                              <li><a href="#">Can View</a></li>
                              <li><a href="#">Can Delete</a></li>
                            </ul>
                          </div>
                
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                        </div>
                        <div class="roledetail tab-pane" id="client">
                          <h6>Permissions: Client</h6>
                          <hr>
                          <div class="dropdown normalmenu">
                            <h5 class="dropdown-toggle" type="button" data-toggle="dropdown">Set project permissions :<span class="caret"></span></h5>
                            <ul class="dropdown-menu">
                              <li><a href="#">Can Add</a></li>
                              <li><a href="#">Can Edit</a></li>
                              <li><a href="#">Can View</a></li>
                              <li><a href="#">Can Delete</a></li>
                            </ul>
                          </div>
                
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="dropdown normalmenu">
                            <h5 class="dropdown-toggle" type="button" data-toggle="dropdown">Set project permissions :<span class="caret"></span></h5>
                            <ul class="dropdown-menu">
                              <li><a href="#">Can Add</a></li>
                              <li><a href="#">Can Edit</a></li>
                              <li><a href="#">Can View</a></li>
                              <li><a href="#">Can Delete</a></li>
                            </ul>
                          </div>
                
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                        </div>
                        <div class="roledetail tab-pane" id="graphic">
                          <h6>Permissions: Graphic Designer</h6>
                          <hr>
                          <div class="dropdown normalmenu">
                            <h5 class="dropdown-toggle" type="button" data-toggle="dropdown">Set project permissions :<span class="caret"></span></h5>
                            <ul class="dropdown-menu">
                              <li><a href="#">Can Add</a></li>
                              <li><a href="#">Can Edit</a></li>
                              <li><a href="#">Can View</a></li>
                              <li><a href="#">Can Delete</a></li>
                            </ul>
                          </div>
                
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="dropdown normalmenu">
                            <h5 class="dropdown-toggle" type="button" data-toggle="dropdown">Set project permissions :<span class="caret"></span></h5>
                            <ul class="dropdown-menu">
                              <li><a href="#">Can Add</a></li>
                              <li><a href="#">Can Edit</a></li>
                              <li><a href="#">Can View</a></li>
                              <li><a href="#">Can Delete</a></li>
                            </ul>
                          </div>
                
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                        </div>
                        <div class="roledetail tab-pane" id="UI">
                          <h6>Permissions: UI/UX Designer</h6>
                          <hr>
                          <div class="dropdown normalmenu">
                            <h5 class="dropdown-toggle" type="button" data-toggle="dropdown">Set project permissions :<span class="caret"></span></h5>
                            <ul class="dropdown-menu">
                              <li><a href="#">Can Add</a></li>
                              <li><a href="#">Can Edit</a></li>
                              <li><a href="#">Can View</a></li>
                              <li><a href="#">Can Delete</a></li>
                            </ul>
                          </div>
                
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="dropdown normalmenu">
                            <h5 class="dropdown-toggle" type="button" data-toggle="dropdown">Set project permissions :<span class="caret"></span></h5>
                            <ul class="dropdown-menu">
                              <li><a href="#">Can Add</a></li>
                              <li><a href="#">Can Edit</a></li>
                              <li><a href="#">Can View</a></li>
                              <li><a href="#">Can Delete</a></li>
                            </ul>
                          </div>
                
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                        </div>
                        <div class="roledetail tab-pane" id="web">
                          <h6>Permissions: Web Designer</h6>
                          <hr>
                          <div class="dropdown normalmenu">
                            <h5 class="dropdown-toggle" type="button" data-toggle="dropdown">Set project permissions :<span class="caret"></span></h5>
                            <ul class="dropdown-menu">
                              <li><a href="#">Can Add</a></li>
                              <li><a href="#">Can Edit</a></li>
                              <li><a href="#">Can View</a></li>
                              <li><a href="#">Can Delete</a></li>
                            </ul>
                          </div>
                
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="dropdown normalmenu">
                            <h5 class="dropdown-toggle" type="button" data-toggle="dropdown">Set project permissions :<span class="caret"></span></h5>
                            <ul class="dropdown-menu">
                              <li><a href="#">Can Add</a></li>
                              <li><a href="#">Can Edit</a></li>
                              <li><a href="#">Can View</a></li>
                              <li><a href="#">Can Delete</a></li>
                            </ul>
                          </div>
                
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                        </div>
                        <div class="roledetail tab-pane" id="seo">
                          <h6>Permissions: Web Seo</h6>
                          <hr>
                          <div class="dropdown normalmenu">
                            <h5 class="dropdown-toggle" type="button" data-toggle="dropdown">Set project permissions :<span class="caret"></span></h5>
                            <ul class="dropdown-menu">
                              <li><a href="#">Can Add</a></li>
                              <li><a href="#">Can Edit</a></li>
                              <li><a href="#">Can View</a></li>
                              <li><a href="#">Can Delete</a></li>
                            </ul>
                          </div>
                
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="dropdown normalmenu">
                            <h5 class="dropdown-toggle" type="button" data-toggle="dropdown">Set project permissions :<span class="caret"></span></h5>
                            <ul class="dropdown-menu">
                              <li><a href="#">Can Add</a></li>
                              <li><a href="#">Can Edit</a></li>
                              <li><a href="#">Can View</a></li>
                              <li><a href="#">Can Delete</a></li>
                            </ul>
                          </div>
                
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                        </div>
                        <div class="roledetail tab-pane" id="quality">
                          <h6>Permissions: Quality Assurance</h6>
                          <hr>
                          <div class="dropdown normalmenu">
                            <h5 class="dropdown-toggle" type="button" data-toggle="dropdown">Set project permissions :<span class="caret"></span></h5>
                            <ul class="dropdown-menu">
                              <li><a href="#">Can Add</a></li>
                              <li><a href="#">Can Edit</a></li>
                              <li><a href="#">Can View</a></li>
                              <li><a href="#">Can Delete</a></li>
                            </ul>
                          </div>
                
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="dropdown normalmenu">
                            <h5 class="dropdown-toggle" type="button" data-toggle="dropdown">Set project permissions :<span class="caret"></span></h5>
                            <ul class="dropdown-menu">
                              <li><a href="#">Can Add</a></li>
                              <li><a href="#">Can Edit</a></li>
                              <li><a href="#">Can View</a></li>
                              <li><a href="#">Can Delete</a></li>
                            </ul>
                          </div>
                
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> Check me out
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<div class="modal fade in" id="addmodel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Edit role</h4>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <label for="inputEmail3" class="col-sm-2 control-label">Title</label>

          <div class="col-sm-10">
            <input type="text" class="form-control" id="inputName" placeholder="Title">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
  <script>
  $(function () {
    /*$('#dataexample1').DataTable();*/
    $('#dataexample2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });
  });
</script>

<style>
 .tableicon {padding-right: 10px;}
 .setting-back ul li {
    width: 100%;
    display: block;
    cursor: pointer;
}
.setting-back .nav-tabs>li.active>a, .setting-back .nav-tabs>li.active>a:focus, .setting-back .nav-tabs>li.active>a:hover {
    color: #3c8dbc;;
    cursor: default;
    background-color: transparent;
    border: 1px solid transparent;
    border-bottom-color: transparent;
}
.setting-back .nav-tabs>li>a:hover, .setting-back .nav-tabs>li>a:active, .setting-back .nav-tabs>li>a:focus {
    color: #3c8dbc;
    background: transparent;
}
.nav-tabs>li>a:hover {
    border-color: transparent;
}
/*.setting-back {
    background: #fff;
}*/
.contab {
    background-color: #ffffff;
    box-shadow: 0px 0px 5px #ddd;
}
.tab-pane {
    padding: 20px;
}
.setting-back h6 {
    font-size: 18px;
    color: #333;
    font-weight: 500;
}
.setting-back ul li a {
    color: #444;
    cursor: pointer;
}
.setting-back ul li a.ger:after {
    content: '';
    display: block;
    width: 0;
    height: 2px;
    background: #3c8dbc;
    transition: width .3s;
    margin-top:10px;
}
.setting-back ul li a.ger:hover::after {
    width: 100%;
}
.setting-back ul li.active a.ger::after {
    width: 100%;
}
.custom_width {
    width: 40%;
}
.labelweight {
  font-weight: lighter;
}
.taberole {
    background: #fff;
    padding: 0px;
    float: left;
    width: 100%;
    box-shadow: 0px 0px 5px #ddd;
}
.taberole li {
    border-top: 1px solid #ddd;
    padding: 10px;
    cursor: pointer;
}
.add_role {
    border: 1px solid #ddd;
    padding: 5px;
    font-size: 12px;
    color: #666;
    float: right;
    cursor: pointer;
}
.taberole h6 {
    font-size: 18px;
    color: #333;
    font-weight: 500;
    float: left;
    width: 100%;
    padding: 0px 10px;
}
.taberole ul {
    padding: 0px;
    float: left;
    width: 100%;
}
.roledetail {
  background-color: #ffffff;
  padding: 10px;
  box-shadow: 0px 0px 5px #ddd;
}
.rolei {
    border: 1px solid #999 !important;
    border-radius: 50% !important;
    height: 25px;
    width: 25px;
    display: inline-block !important;
    text-align: center;
    line-height: 25px !important;
    padding: 0px !important;
}
.taberole li.ger {
    column-count: 2;
}
.taberole li.ger.active {
    background: #3c8dbc;
    color: #fff;
}
.taberole li.ger.active .rolei {
    background: #fff;
    color: #999;
    border:1px solid #fff !important;
    cursor: pointer;
}
.taberole li.ger.active .rolei:hover, .taberole li.ger.active .rolei:active, .taberole li.ger.active .rolei:focus {
    background: #fff;
    color: #3c8dbc;
    border:1px solid #fff !important;
    cursor: pointer;
}
.taberole li.ger ul {
    text-align: right;
}
.taberole li.ger li {
    display: inline;
    padding: 2px;
    border: none;
}
.nav-tabs {
  border-bottom: none;
}
.normalmenu h5 {
    margin: 0px;
    cursor: pointer;
    font-size: 14px;
    font-weight: 600;
    color: #444;
}
@media screen and (max-width: 767px) {
  .taberole {
    position: relative;
    z-index: 1;
  }
}
</style>
@endsection