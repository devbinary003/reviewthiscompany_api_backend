<html>
<head>
</head>
<body style="font-family: Arial; font-size: 12px;">
<div>
	<p style="font-size: 24px;">Hii, {{ $email }}
    <p>
        Your Account Activation link Is here, please follow the link below to activate your Account.
    </p>
   

    <p>
        <a href="{{ env('REVIEWGROWTH_URL') }}/login/?active_code={{$active_code}}">
            Follow this link to Activate your Account.
        </a>
    </p>
</div>
</body>
</html>