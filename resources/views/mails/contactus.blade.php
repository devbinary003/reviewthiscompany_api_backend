<html>
<head>
</head>
<body style="font-family: Arial; font-size: 12px;">
<div>
	<p style="font-size: 24px;">Hii, Admin</p>
    <p>
        <b>Mail From</b> : {{ $user_name }}
    </p>
    <p>
        <b>Subject</b> : {{ $contact_subject }}
    </p>
    <p>
        <b>Message</b> : {{ $text }}
    </p>
    <p>
        <b>{{ $user_name }}'s Phone Number</b> : {{ $phone }}
    </p>
</div>
</body>
</html>