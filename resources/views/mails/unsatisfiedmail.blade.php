<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
      <title>review</title>
      <style type="text/css">
         body{
         width:100%;
         background-color:#ffffff;
         margin:0;
         padding:0;
         -webkit-font-smoothing:antialiased;
         mso-margin-top-alt:0;
         mso-margin-bottom-alt:0;
         mso-padding-alt:0 0 0 0;
         }
         p,h1,h2,h3,h4{
         margin-top:0;
         margin-bottom:0;
         padding-top:0;
         padding-bottom:0;
         }
         span.preheader{
         display:none;
         font-size:1px;
         }
         html{
         width:100%;
         }
         table{
         font-size:12px;
         border:0;
         }
         .menu-space{
         padding-right:25px;
         }
         @media only screen and (max-width:640px){
         body{
         width:auto !important;
         }
         }  @media only screen and (max-width:640px){
         body[yahoo] .main{
         width:440px !important;
         }
         }  @media only screen and (max-width:640px){
         body[yahoo] .two-left{
         width:420px !important;
         margin:0 auto;
         }
         }  @media only screen and (max-width:640px){
         body[yahoo] .full{
         width:100% !important;
         margin:0 auto;
         }
         }  @media only screen and (max-width:640px){
         body[yahoo] .alaine{
         text-align:center;
         }
         }  @media only screen and (max-width:640px){
         body[yahoo] .menu-space{
         padding-right:0;
         }
         }  @media only screen and (max-width:640px){
         body[yahoo] .banner{
         width:438px !important;
         }
         }  @media only screen and (max-width:640px){
         body[yahoo] .menu{
         width:438px !important;
         margin:0 auto;
         border-bottom:#e1e0e2 solid 1px;
         }
         }  @media only screen and (max-width:640px){
         body[yahoo] .date{
         width:438px !important;
         margin:0 auto;
         text-align:center;
         }
         }  @media only screen and (max-width:640px){
         body[yahoo] .two-left-inner{
         width:400px !important;
         margin:0 auto;
         }
         }  @media only screen and (max-width:640px){
         body[yahoo] .menu-icon{
         display:block;
         }
         }  @media only screen and (max-width:640px){
         body[yahoo] .two-left-menu{
         text-align:center;
         }
         }  @media only screen and (max-width:479px){
         body{
         width:auto !important;
         }
         }  @media only screen and (max-width:479px){
         body[yahoo] .main{
         width:310px !important;
         }
         }  @media only screen and (max-width:479px){
         body[yahoo] .two-left{
         width:300px !important;
         margin:0 auto;
         }
         }  @media only screen and (max-width:479px){
         body[yahoo] .full{
         width:100% !important;
         margin:0 auto;
         }
         }  @media only screen and (max-width:479px){
         body[yahoo] .alaine{
         text-align:center;
         }
         }  @media only screen and (max-width:479px){
         body[yahoo] .menu-space{
         padding-right:0;
         }
         }  @media only screen and (max-width:479px){
         body[yahoo] .banner{
         width:308px !important;
         }
         }  @media only screen and (max-width:479px){
         body[yahoo] .menu{
         width:308px !important;
         margin:0 auto;
         border-bottom:#e1e0e2 solid 1px;
         }
         }  @media only screen and (max-width:479px){
         body[yahoo] .date{
         width:308px !important;
         margin:0 auto;
         text-align:center;
         }
         }  @media only screen and (max-width:479px){
         body[yahoo] .two-left-inner{
         width:280px !important;
         margin:0 auto;
         }
         }  @media only screen and (max-width:479px){
         body[yahoo] .menu-icon{
         display:none;
         }
         }  @media only screen and (max-width:479px){
         body[yahoo] .two-left-menu{
         width:310px !important;
         margin:0 auto;
         }
         }
      </style>
   </head>
   <body yahoo="fix" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="width:100%; background-color:#ffffff; margin:0; padding:0; -webkit-font-smoothing:antialiased; mso-margin-top-alt:0; mso-margin-bottom-alt:0; mso-padding-alt:0 0 0 0;">
      
      <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#e6e7e8" style="background:#e6e7e8; font-size:12px; border:0;">
         <tr>
            <td align="center" valign="top">
              
               <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="font-size:12px; border:0;">
                  <tr>
                     <td align="center" valign="top" bgcolor="#fafafa">
                        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="main" style="font-size:12px; border:0;">
                           <tr>
                              <td align="center" valign="middle" bgcolor="#4582e9" style="background:#4582e9;padding-top:25px;font-family:'Myriad Pro Condensed', Arial, Helvetica, sans-serif;font-size:45px;color:#ffffff;font-weight:bold;letter-spacing:-2px;">{{$business_name}}</td>
                           </tr>
                           <tr>
                              <td align="center" valign="middle" bgcolor="#4582e9" style="font-family:'Myriad Pro Condensed', Arial, Helvetica, sans-serif;font-size:18px;color:#ffffff; padding-bottom:20px;">{{$business_address}} </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
               
               <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="font-size:12px; border:0;">
                  <tr>
                     <td align="center" valign="top" bgcolor="#fafafa">
                        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="main" style="font-size:12px; border:0;">
                           <tr>
                              <td align="center" valign="top" bgcolor="#e9e9e9" style="background:#ffffff;">
                                 <table width="565" border="0" align="center" cellpadding="0" cellspacing="0" class="two-left" style="font-size:12px; border:0;">
                                    <tr>
                                       <td height="45" align="left" valign="top" bgcolor="#FFFFFF" style="font-family:'Myriad Pro', Arial, Helvetica, sans-serif;font-size:15px;color:#535353;padding-bottom:10px;padding-left:25px;padding-right:10px;padding-top:20px;">
                                          <span style="font-size:17px; color:#535353; font-weight:bold;">Hi {{$reviewer_name}},</span>
                                          <br>
                                          <br>
                                          {{$unsc_emsg}}
                                       </td>
                                    </tr>
                                    
                                 </table>
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
              
               <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="font-size:12px; border:0;">
                  <tr>
                     <td align="center" valign="top" bgcolor="#fafafa">
                        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="main">
                           <tr>
                              <td align="center" valign="top" bgcolor="#0030A5" style="background:#4582e9;padding-top:20px;padding-bottom:20px;">
                                 <table width="540" border="0" align="center" cellpadding="0" cellspacing="0" class="two-left" style="font-size:12px; border:0;">
                                    <tr>
                                       <td height="45" align="center" valign="top" style="font-family:'Myriad Pro', Arial, Helvetica, sans-serif;font-size:13px;color:#ffffff;padding-bottom:20px;padding-left:10px;padding-right:20px;">
                                          <a href="mailto:unsubscribe@reviewgrowth.com" style="color:#FFFFFF;text-decoration:none;">Unsubscribe</a>
                                          <br>
                                          <br>
                                          ReviewThisCompany © Copyright 2019
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>

            </td>
         </tr>
      </table>
     
   </body>
</html>

