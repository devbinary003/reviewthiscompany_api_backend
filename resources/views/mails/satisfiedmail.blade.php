<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
      <title>review</title>
      <style type="text/css">
         body{
         width:100%;
         background-color:#ffffff;
         margin:0;
         padding:0;
         -webkit-font-smoothing:antialiased;
         mso-margin-top-alt:0;
         mso-margin-bottom-alt:0;
         mso-padding-alt:0 0 0 0;
         }
         p,h1,h2,h3,h4{
         margin-top:0;
         margin-bottom:0;
         padding-top:0;
         padding-bottom:0;
         }
         span.preheader{
         display:none;
         font-size:1px;
         }
         html{
         width:100%;
         }
         table{
         font-size:12px;
         border:0;
         }
         .menu-space{
         padding-right:25px;
         }
         @media only screen and (max-width:640px){
         body{
         width:auto !important;
         }
         }  @media only screen and (max-width:640px){
         body[yahoo] .main{
         width:440px !important;
         }
         }  @media only screen and (max-width:640px){
         body[yahoo] .two-left{
         width:420px !important;
         margin:0 auto;
         }
         }  @media only screen and (max-width:640px){
         body[yahoo] .full{
         width:100% !important;
         margin:0 auto;
         }
         }  @media only screen and (max-width:640px){
         body[yahoo] .alaine{
         text-align:center;
         }
         }  @media only screen and (max-width:640px){
         body[yahoo] .menu-space{
         padding-right:0;
         }
         }  @media only screen and (max-width:640px){
         body[yahoo] .banner{
         width:438px !important;
         }
         }  @media only screen and (max-width:640px){
         body[yahoo] .menu{
         width:438px !important;
         margin:0 auto;
         border-bottom:#e1e0e2 solid 1px;
         }
         }  @media only screen and (max-width:640px){
         body[yahoo] .date{
         width:438px !important;
         margin:0 auto;
         text-align:center;
         }
         }  @media only screen and (max-width:640px){
         body[yahoo] .two-left-inner{
         width:400px !important;
         margin:0 auto;
         }
         }  @media only screen and (max-width:640px){
         body[yahoo] .menu-icon{
         display:block;
         }
         }  @media only screen and (max-width:640px){
         body[yahoo] .two-left-menu{
         text-align:center;
         }
         }  @media only screen and (max-width:479px){
         body{
         width:auto !important;
         }
         }  @media only screen and (max-width:479px){
         body[yahoo] .main{
         width:310px !important;
         }
         }  @media only screen and (max-width:479px){
         body[yahoo] .two-left{
         width:300px !important;
         margin:0 auto;
         }
         }  @media only screen and (max-width:479px){
         body[yahoo] .full{
         width:100% !important;
         margin:0 auto;
         }
         }  @media only screen and (max-width:479px){
         body[yahoo] .alaine{
         text-align:center;
         }
         }  @media only screen and (max-width:479px){
         body[yahoo] .menu-space{
         padding-right:0;
         }
         }  @media only screen and (max-width:479px){
         body[yahoo] .banner{
         width:308px !important;
         }
         }  @media only screen and (max-width:479px){
         body[yahoo] .menu{
         width:308px !important;
         margin:0 auto;
         border-bottom:#e1e0e2 solid 1px;
         }
         }  @media only screen and (max-width:479px){
         body[yahoo] .date{
         width:308px !important;
         margin:0 auto;
         text-align:center;
         }
         }  @media only screen and (max-width:479px){
         body[yahoo] .two-left-inner{
         width:280px !important;
         margin:0 auto;
         }
         }  @media only screen and (max-width:479px){
         body[yahoo] .menu-icon{
         display:none;
         }
         }  @media only screen and (max-width:479px){
         body[yahoo] .two-left-menu{
         width:310px !important;
         margin:0 auto;
         }
         }
      </style>
   </head>
   <body yahoo="fix" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="width:100%; background-color:#ffffff; margin:0; padding:0; -webkit-font-smoothing:antialiased; mso-margin-top-alt:0; mso-margin-bottom-alt:0; mso-padding-alt:0 0 0 0;">
      
      <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#e6e7e8" style="background:#e6e7e8; font-size:12px; border:0;">
         <tr>
            <td align="center" valign="top">
              
               <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="font-size:12px; border:0;">
                  <tr>
                     <td align="center" valign="top" bgcolor="#fafafa">
                        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="main" style="font-size:12px; border:0;">
                           <tr>
                              <td align="center" valign="middle" bgcolor="#4582e9" style="background:#4582e9;padding-top:25px;font-family:'Myriad Pro Condensed', Arial, Helvetica, sans-serif;font-size:45px;color:#ffffff;font-weight:bold;letter-spacing:-2px;">{{$business_name}}</td>
                           </tr>
                           <tr>
                              <td align="center" valign="middle" bgcolor="#4582e9" style="font-family:'Myriad Pro Condensed', Arial, Helvetica, sans-serif;font-size:18px;color:#ffffff; padding-bottom:20px;">{{$business_address}} </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
               
               <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="font-size:12px; border:0;">
                  <tr>
                     <td align="center" valign="top" bgcolor="#fafafa">
                        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="main" style="font-size:12px; border:0;">
                           <tr>
                              <td align="center" valign="top" bgcolor="#e9e9e9" style="background:#ffffff;">
                                 <table width="565" border="0" align="center" cellpadding="0" cellspacing="0" class="two-left" style="font-size:12px; border:0;">
                                    <tr>
                                       <td height="45" align="left" valign="top" bgcolor="#FFFFFF" style="font-family:'Myriad Pro', Arial, Helvetica, sans-serif;font-size:15px;color:#535353;padding-bottom:10px;padding-left:25px;padding-right:10px;padding-top:20px;">
                                          <span style="font-size:17px; color:#535353; font-weight:bold;">Hi {{$reviewer_name}},</span>
                                          <br>
                                          <br>
                                          Thank you so much for your kind words. We really appreciate it and would love for others to read it too. Help us out by making your review go live on Google!
                                       </td>
                                    </tr>
                                    <tr>
                                       <td height="45" align="left" valign="top" bgcolor="#FFFFFF" style="font-family:'Myriad Pro', Arial, Helvetica, sans-serif;font-size:15px;color:#000000;padding-bottom:30px;padding-left:10px;padding-right:10px;padding-top:20px;">
                                          <table width="520" border="0" align="center" cellpadding="0" cellspacing="0" class="two-left" style="font-size:12px; border:0;">
                                             <tr>
                                                <td align="center" valign="top" bgcolor="#f8f8f8">
                                                   <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="font-size:12px; border:0;">
                                                      <tr>
                                                         <td height="45" align="center" valign="top" bgcolor="#f8f8f8" style="font-family:'Myriad Pro', Arial, Helvetica, sans-serif;font-size:17px;color:#535353; padding:20px;"><span style="font-weight:bold;">Step 1:</span> Copy the review from the box below to add it to your
                                                            clipboard.
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td height="45" align="center" valign="top" bgcolor="#f8f8f8" style="padding-bottom:20px;">
                                                   <table width="470" border="0" align="center" cellpadding="0" cellspacing="0" class="two-left" style="font-size:12px; border:0;">
                                                      <tr>
                                                         <td align="center" valign="top">
                                                            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="font-size:12px; border:0;">
                                                               <tr>
                                                                  <td align="left" valign="top" bgcolor="#666666" style="font-family:'Myriad Pro', Arial, Helvetica, sans-serif;font-size:17px;color:#ffffff;padding-bottom:10px;padding-top:10px;padding-left:10px;border:#3a3a3a solid 2px;">{{$reviewer_description}}</td>
                                                               </tr>
                                                            </table>
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td height="45" align="center" valign="top" bgcolor="#f8f8f8" style="font-family:'Myriad Pro', Arial, Helvetica, sans-serif;font-size:17px;color:#535353; padding:20px;"> <span style="font-weight:bold;">Step 2:</span> Click the button below to open the Google Review box. If
                                                   required, login to your Google account.
                                                </td>
                                             </tr>
                                             <tr>
                                                <td height="45" align="center" valign="top" bgcolor="#f8f8f8"><a href="https://search.google.com/local/writereview?placeid={{$business_page_id}}" target="_blank"><img src="https://gallery.mailchimp.com/0a784ed42ccd7966a8eb80d97/images/a8d8138b-405b-4b0d-8c36-27e8dd052e9f.png" width="237" height="71" border="0" alt="draft_1_03.jpg"></a>                              </td>
                                             </tr>
                                             <tr>
                                                <td height="45" align="center" valign="top" bgcolor="#f8f8f8" style="font-family:'Myriad Pro', Arial, Helvetica, sans-serif;font-size:17px;color:#535353; padding:20px;"><span style="font-weight:bold;">Step 3:</span> Choose your star rating and paste your copied review
                                                   from 
                                                   Step 1. Click the Post button and you’re done!
                                                </td>
                                             </tr>
                                             <tr>
                                                <td height="45" align="left" valign="top" style="font-family:'Myriad Pro', Arial, Helvetica, sans-serif;font-size:15px;color:#535353;padding-bottom:10px;padding-right:20px;padding-top:20px;">Thanks again for taking the time and effort to make sure your kind words are live on Google. It really means a lot to us and we thank you for your patronage. See you soon!                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
              
               <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="font-size:12px; border:0;">
                  <tr>
                     <td align="center" valign="top" bgcolor="#fafafa">
                        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="main">
                           <tr>
                              <td align="center" valign="top" bgcolor="#0030A5" style="background:#4582e9;padding-top:20px;padding-bottom:20px;">
                                 <table width="540" border="0" align="center" cellpadding="0" cellspacing="0" class="two-left" style="font-size:12px; border:0;">
                                    <tr>
                                       <td height="45" align="center" valign="top" style="font-family:'Myriad Pro', Arial, Helvetica, sans-serif;font-size:13px;color:#ffffff;padding-bottom:20px;padding-left:10px;padding-right:20px;">
                                          <a href="mailto:unsubscribe@reviewgrowth.com" style="color:#FFFFFF;text-decoration:none;">Unsubscribe</a>
                                          <br>
                                          <br>
                                          ReviewThisCompany © Copyright 2019
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>

            </td>
         </tr>
      </table>
     
   </body>
</html>

