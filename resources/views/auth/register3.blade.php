@extends('layouts.default')

@section('content')
<div class="">
<div class="register-box">
  <div class="register-logo">
    <a href="https://adminlte.io/themes/AdminLTE/index2.html"><b>Admin</b>LTE</a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Fill security question</p>

    <form action="https://adminlte.io/themes/AdminLTE/index.html" method="post">
      <div class="form-group">
        <label>Select</label>
        <select class="form-control">
          <option>option 1</option>
          <option>option 2</option>
          <option>option 3</option>
          <option>option 4</option>
          <option>option 5</option>
        </select>
      </div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Fill your answer">
        <!-- <span class="glyphicon glyphicon-envelope form-control-feedback"></span> -->
      </div>
      <div class="form-group">
        <p class="help-block">Not more then 2mb</p>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Next</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!-- /.form-box -->
</div>
</div>
<style>
header {display: none;}
.main-sidebar {display: none;}
</style>
@endsection
