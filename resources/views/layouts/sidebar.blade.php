<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" style="height: auto;">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{url('/public')}}/images/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="https://adminlte.io/themes/AdminLTE/#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="https://adminlte.io/themes/AdminLTE/#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu tree" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview menu-open">
          <a href="">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="./dashboard1"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li><a href="./dashboard2"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
          </ul>
        </li>
        <li>
          <a href="./profile">
            <i class="fa fa-book"></i> 
            <span>Profile</span>
          </a>
        </li>
        <li class="treeview active menu-open">
          <a href="">
            <i class="fa fa-envelope"></i> <span>Mailbox</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active">
              <a href="./mail">Inbox
                <span class="pull-right-container">
                  <span class="label label-primary pull-right">13</span>
                </span>
              </a>
            </li>
            <li><a href="./compose_message">Compose</a></li>
            <li><a href="./read_mail">Read</a></li>
          </ul>
        </li>
        <li>
          <a href="./user-list">
            <i class="fa fa-book"></i> 
            <span>User List</span>
          </a>
        </li>
        <li>
          <a href="./post-list">
            <i class="fa fa-book"></i> 
            <span>Post List</span>
          </a>
        </li>
        <li>
          <a href="./setting">
            <i class="fa fa-book"></i> 
            <span>Setting</span>
          </a>
        </li>
        <!-- <li class="treeview">
          <a href="https://adminlte.io/themes/AdminLTE/#">
            <i class="fa fa-table"></i> <span>Tables</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="https://adminlte.io/themes/AdminLTE/pages/tables/simple.html"><i class="fa fa-circle-o"></i> Simple tables</a></li>
            <li><a href="https://adminlte.io/themes/AdminLTE/pages/tables/data.html"><i class="fa fa-circle-o"></i> Data tables</a></li>
            <li><a href="https://adminlte.io/themes/AdminLTE/pages/tables/simple.html"><i class="fa fa-circle-o"></i> Simple tables</a></li>
            <li><a href="https://adminlte.io/themes/AdminLTE/pages/tables/data.html"><i class="fa fa-circle-o"></i> Data tables</a></li>
          </ul>
        </li> -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
<style>
  ul.sidebar-menu.tree li.bg-success:last-child {
    display: none;
  }
</style>