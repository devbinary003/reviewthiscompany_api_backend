<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>{{ config('app.name', 'Admin Lite') }}</title>


    <!-- jQuery 3 --> 
    <!-- <script src="{{ url('/public') }}/js/jquery.min.js"></script> -->
    <!-- Bootstrap 3.3.7 -->
    <!-- <script src="{{ url('/public') }}/js/bootstrap.min.js"></script> -->
    <!-- iCheck -->
    <script src="{{ url('/public') }}/js/icheck.min.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Bootstrap 3.3.7 -->
 <!--    <link rel="stylesheet" href="{{ url('/public') }}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ url('/public') }}/css/dataTables.bootstrap.min.css"> -->

       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="https://unpkg.com/ionicons@4.4.4/dist/css/ionicons.min.css" rel="stylesheet">

    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ url('/public') }}/css/ionicons.min.css">

<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionic/1.3.2/css/ionic.min.css"> -->

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ url('/public') }}/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ url('/public') }}/css/blue.css">

    <link rel="stylesheet" href="{{ url('/public') }}/css/allskins.min.css">

    <link rel="stylesheet" href="{{ url('/public') }}/css/morris.css">

    <link rel="stylesheet" href="{{ url('/public') }}/css/jquery-jvectormap.css">

    <link rel="stylesheet" href="{{ url('/public') }}/css/bootstrap-datepicker.min.css">

    <link rel="stylesheet" href="{{ url('/public') }}/css/daterangepicker.css">

    <link rel="stylesheet" href="{{ url('/public') }}/css/bootstrap3-wysihtml5.min.css">




</head>

<body class="hold-transition login-page hold-transition skin-blue sidebar-mini hold-transition register-page">
<div class="wrapper" style="height: auto; min-height: 100%;">
   @include('layouts.header')
   @include('layouts.sidebar')
   @yield('content')

   @include('layouts.footer')

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</div>
</body>
</html>

