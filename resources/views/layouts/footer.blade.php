
	<footer>
        <div class="copyright_sc">
            <div class="container">
                <p>Copyright <?php echo date('Y');?>, AppName. All Rights Reserved</p>
            </div>
        </div>
    </footer>
<!-- jQuery 3 -->
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<!-- jQuery 3 -->
<script src="{{ url('/public') }}/js/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ url('/public') }}/js/jquery-ui.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ url('/public') }}/js/bootstrap.min.js"></script>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js "></script>

<!-- Morris.js charts -->
<script src="{{ url('/public') }}/js/raphael.min.js"></script>
<script src="{{ url('/public') }}/js/morris.min.js"></script>
<!-- Sparkline -->
<script src="{{ url('/public') }}/js/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="{{ url('/public') }}/js/jquery-jvectormap-1.2.2.min.js"></script>
<script src="{{ url('/public') }}/js/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="{{ url('/public') }}/js/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="{{ url('/public') }}/js/moment.min.js"></script>
<script src="{{ url('/public') }}/js/daterangepicker.js"></script>
<!-- datepicker -->
<script src="{{ url('/public') }}/js/bootstrap-datepicker.min.js"></script>
<!-- Slimscroll -->
<script src="{{ url('/public') }}/js/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="{{ url('/public') }}/js/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="{{ url('/public') }}/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ url('/public') }}/js/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ url('/public') }}/js/demo.js"></script>

<script src="{{ url('/public') }}/js/select2.js"></script>

<script src="{{ url('/public') }}/js/select2.full.min.js"></script>

<script src="{{ url('/public') }}/js/jquery.inputmask.js"></script>

<script src="{{ url('/public') }}/js/jquery.inputmask.date.extensions.js"></script>

<script src="{{ url('/public') }}/js/jquery.inputmask.extensions.js"></script>

<script src="{{ url('/public') }}/js/bootstrap3-wysihtml5.all.min.js"></script>

