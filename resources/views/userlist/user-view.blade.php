@extends('layouts.default')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 1170.3px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User view
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">User profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">User view</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal">
              <div class="row">
              <div class="col-sm-6">
                <div class="box-body">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Name <span class="dott"> :</span></label>

                    <div class="col-sm-9">
                      <p class="view-info"> Nina Mcintire</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Email <span class="dott"> :</span></label>

                    <div class="col-sm-9">
                      <p class="view-info"> abc@gmail.com</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Image <span class="dott"> :</span></label>
                    <div class="col-sm-9">
                      <img src="./public/images/default-50x50.gif" alt="" class="profile-image">
                    </div>
                  </div>
                  
                </div>
              </div>
              <div class="col-sm-6">
                <div class="box-body">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Address <span class="dott">:</span></label>
 
                    <div class="col-sm-9">
                      <p class="view-info"> Binary Data, Punjab, Mohali</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Phone No. <span class="dott">:</span></label>

                    <div class="col-sm-9">
                      <p class="view-info"> 1234567890</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Textarea <span class="dott">: </span></label>
                    <div class="col-sm-9">
                      <p class="view-info"> Lorem ipsum represents a long-held tradition for designers, typographers and the like. Some people hate it and argue for its demise, but others ignore the hate as they create awesome tools to help create filler text for everyone from bacon lovers to Charlie Sheen fans.</p>
                    </div>
                  </div>
                  
                </div>
              </div>
                <!-- /.box-body -->
            <!-- <div class="row">
              <div class="col-sm-12">
                <div class="box-footer">
                  <button type="submit" class="btn btn-default">Cancel</button>
                  <button type="submit" class="btn btn-info pull-right">Sign in</button>
                </div>
              </div>
            </div> -->
                <!-- /.box-footer -->
              </div>
            </form>
          </div>
       
      <!-- /.row -->
  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<style type="text/css">
  .profile-image {
    height: 80px;
    width: 80px;
}
.view-info {
    margin: 0px;
    padding-top: 7px;
    color: #555;
}
</style>
  @endsection