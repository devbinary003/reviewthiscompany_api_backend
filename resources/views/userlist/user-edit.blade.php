@extends('layouts.default')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 1170.3px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User edit
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">User profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">User edit</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal">
              <div class="col-sm-6">
                <div class="box-body">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Name</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="inputName" placeholder="Email">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Email</label>

                    <div class="col-sm-9">
                      <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-9">
                      <label for="exampleInputFile">File input</label>
                      <input type="file" id="exampleInputFile">

                      <p class="help-block">Not more then 2mb</p>
                    </div>
                  </div>
                  
                </div>
              </div>
              <div class="col-sm-6">
                <div class="box-body">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Address</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="inputName" placeholder="Email">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Phone No.</label>

                    <div class="col-sm-9">
                      <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Textarea</label>
                    <div class="col-sm-9">
                      <textarea class="form-control" rows="3" placeholder="Enter ..." data-gramm="true" data-txt_gramm_id="468e8b02-5c8a-6217-d5b6-2c9596d9b903" data-gramm_id="468e8b02-5c8a-6217-d5b6-2c9596d9b903" spellcheck="false" data-gramm_editor="true"></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-9">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox"> Remember me
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
                <!-- /.box-body -->
            <div class="row">
              <div class="col-sm-12">
                <div class="box-footer">
                  <button type="submit" class="btn btn-default">Cancel</button>
                  <button type="submit" class="btn btn-info pull-right">Sign in</button>
                </div>
              </div>
            </div>
                <!-- /.box-footer -->
              
            </form>
          </div>
       
      <!-- /.row -->
  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @endsection