@extends('layouts.default')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 960px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Post List
        <small>all post</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="https://adminlte.io/themes/AdminLTE/pages/tables/data.html#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="https://adminlte.io/themes/AdminLTE/pages/tables/data.html#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Post list</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row"><div class="col-sm-6"><div class="dataTables_length" id="example1_length">
                  <label>Show
                    <select name="example1_length" aria-controls="example1" class="form-control input-sm"><option value="10">10</option>
                    <option value="25">25</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                    </select> entries
                  </label>
                </div>
              </div>

              <div class="col-sm-6">
                <div id="example1_filter" class="dataTables_filter">
                  <label>Search:<input type="search" class="form-control input-sm" placeholder="" aria-controls="example1"></label>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-12">
                <table id="dataexample1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                <thead>
                <tr role="row">
                  <th class="sorting_asc" tabindex="0" aria-controls="dataexample1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 110px;">Sr.No.</th>
                  <th class="sorting" tabindex="0" aria-controls="dataexample1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 190px;">User Name</th>
                  <th class="sorting" tabindex="0" aria-controls="dataexample1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 190px;">Created Date</th>
                  <th class="sorting" tabindex="0" aria-controls="dataexample1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 190px;">Created By</th>
                  <th class="sorting" tabindex="0" aria-controls="dataexample1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 195px;">Status</th>
                </tr>
                </thead>
                <tbody>
                
               
                <tr role="row" class="odd">
                  <td class="sorting_1">1</td>
                  <td>Firefox 1.0</td>
                  <td>Win 98+ / OSX.2+</td>
                  <td>1.7</td>
                  <td>
                    <a href="#" class="tableicon" data-toggle="modal" data-target="#modal-default"><span class="glyphicon glyphicon-edit"></span></a>
                    <a href="#" class="tableicon" data-toggle="modal" data-target="#modal-view"><span class="glyphicon glyphicon-eye-open"></span></a>
                    <a href="#" class="tableicon"><span class="glyphicon glyphicon-trash"></span></a>
                  </td>
                </tr><tr role="row" class="even">
                  <td class="sorting_1">2</td>
                  <td>Firefox 1.5</td>
                  <td>Win 98+ / OSX.2+</td>
                  <td>1.8</td>
                  <td>
                    <a href="#" class="tableicon" data-toggle="modal" data-target="#modal-default"><span class="glyphicon glyphicon-edit"></span></a>
                    <a href="#" class="tableicon" data-toggle="modal" data-target="#modal-view"><span class="glyphicon glyphicon-eye-open"></span></a>
                    <a href="#" class="tableicon"><span class="glyphicon glyphicon-trash"></span></a>
                  </td>
                </tr><tr role="row" class="odd">
                  <td class="sorting_1">3</td>
                  <td>Firefox 2.0</td>
                  <td>Win 98+ / OSX.2+</td>
                  <td>1.8</td>
                  <td>
                    <a href="#" class="tableicon" data-toggle="modal" data-target="#modal-default"><span class="glyphicon glyphicon-edit"></span></a>
                    <a href="#" class="tableicon" data-toggle="modal" data-target="#modal-view"><span class="glyphicon glyphicon-eye-open"></span></a>
                    <a href="#" class="tableicon"><span class="glyphicon glyphicon-trash"></span></a>
                  </td>
                </tr><tr role="row" class="even">
                  <td class="sorting_1">4</td>
                  <td>Firefox 3.0</td>
                  <td>Win 2k+ / OSX.3+</td>
                  <td>1.9</td>
                  <td>
                    <a href="#" class="tableicon" data-toggle="modal" data-target="#modal-default"><span class="glyphicon glyphicon-edit"></span></a>
                    <a href="#" class="tableicon" data-toggle="modal" data-target="#modal-view"><span class="glyphicon glyphicon-eye-open"></span></a>
                    <a href="#" class="tableicon"><span class="glyphicon glyphicon-trash"></span></a>
                  </td>
                </tr><tr role="row" class="odd">
                  <td class="sorting_1">5</td>
                  <td>Camino 1.0</td>
                  <td>OSX.2+</td>
                  <td>1.8</td>
                  <td>
                    <a href="#" class="tableicon" data-toggle="modal" data-target="#modal-default"><span class="glyphicon glyphicon-edit"></span></a>
                    <a href="#" class="tableicon" data-toggle="modal" data-target="#modal-view"><span class="glyphicon glyphicon-eye-open"></span></a>
                    <a href="#" class="tableicon"><span class="glyphicon glyphicon-trash"></span></a>
                  </td>
                </tr><tr role="row" class="even">
                  <td class="sorting_1">6</td>
                  <td>Camino 1.5</td>
                  <td>OSX.3+</td>
                  <td>1.8</td>
                  <td>
                    <a href="#" class="tableicon" data-toggle="modal" data-target="#modal-default"><span class="glyphicon glyphicon-edit"></span></a>
                    <a href="#" class="tableicon" data-toggle="modal" data-target="#modal-view"><span class="glyphicon glyphicon-eye-open"></span></a>
                    <a href="#" class="tableicon"><span class="glyphicon glyphicon-trash"></span></a>
                  </td>
                </tr><tr role="row" class="odd">
                  <td class="sorting_1">7</td>
                  <td>Netscape 7.2</td>
                  <td>Win 95+ / Mac OS 8.6-9.2</td>
                  <td>1.7</td>
                  <td>
                    <a href="#" class="tableicon" data-toggle="modal" data-target="#modal-default"><span class="glyphicon glyphicon-edit"></span></a>
                    <a href="#" class="tableicon" data-toggle="modal" data-target="#modal-view"><span class="glyphicon glyphicon-eye-open"></span></a>
                    <a href="#" class="tableicon"><span class="glyphicon glyphicon-trash"></span></a>
                  </td>
                </tr><tr role="row" class="even">
                  <td class="sorting_1">8</td>
                  <td>Netscape Browser 8</td>
                  <td>Win 98SE+</td>
                  <td>1.7</td>
                  <td>
                    <a href="#" class="tableicon" data-toggle="modal" data-target="#modal-default"><span class="glyphicon glyphicon-edit"></span></a>
                    <a href="#" class="tableicon" data-toggle="modal" data-target="#modal-view"><span class="glyphicon glyphicon-eye-open"></span></a>
                    <a href="#" class="tableicon"><span class="glyphicon glyphicon-trash"></span></a>
                  </td>
                </tr><tr role="row" class="odd">
                  <td class="sorting_1">9</td>
                  <td>Netscape Navigator 9</td>
                  <td>Win 98+ / OSX.2+</td>
                  <td>1.8</td>
                  <td>
                    <a href="#" class="tableicon" data-toggle="modal" data-target="#modal-default"><span class="glyphicon glyphicon-edit"></span></a>
                    <a href="#" class="tableicon" data-toggle="modal" data-target="#modal-view"><span class="glyphicon glyphicon-eye-open"></span></a>
                    <a href="#" class="tableicon"><span class="glyphicon glyphicon-trash"></span></a>
                  </td>
                </tr><tr role="row" class="even">
                  <td class="sorting_1">10</td>
                  <td>Mozilla 1.0</td>
                  <td>Win 95+ / OSX.1+</td>
                  <td>1</td>
                  <td>
                    <a href="#" class="tableicon" data-toggle="modal" data-target="#modal-default"><span class="glyphicon glyphicon-edit"></span></a>
                    <a href="#" class="tableicon" data-toggle="modal" data-target="#modal-view"><span class="glyphicon glyphicon-eye-open"></span></a>
                    <a href="#" class="tableicon"><span class="glyphicon glyphicon-trash"></span></a>
                  </td>
                </tr></tbody>
                <tfoot>
                <tr>
                  <th rowspan="1" colspan="1">Sr.No.</th><th rowspan="1" colspan="1">Post Name</th><th rowspan="1" colspan="1">Created Date</th><th rowspan="1" colspan="1">Created By</th><th rowspan="1" colspan="1">Status</th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-5">
              <div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div>
            </div>
            <div class="col-sm-7">
              <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                <ul class="pagination">
                  <li class="paginate_button previous disabled" id="example1_previous">
                    <a href="https://adminlte.io/themes/AdminLTE/pages/tables/data.html#" aria-controls="example1" data-dt-idx="0" tabindex="0">Previous</a></li>
                    <li class="paginate_button active"><a href="https://adminlte.io/themes/AdminLTE/pages/tables/data.html#" aria-controls="example1" data-dt-idx="1" tabindex="0">1</a></li>
                    <li class="paginate_button "><a href="https://adminlte.io/themes/AdminLTE/pages/tables/data.html#" aria-controls="example1" data-dt-idx="2" tabindex="0">2</a></li>
                    <li class="paginate_button next" id="example1_next"><a href="https://adminlte.io/themes/AdminLTE/pages/tables/data.html#" aria-controls="example1" data-dt-idx="7" tabindex="0">Next</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script>
  $(function () {
    /*$('#dataexample1').DataTable();*/
    $('#dataexample2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });
  });
</script>
<!-- Modal -->
<div class="modal fade in" id="modal-default">
  <div class="modal-dialog postedit">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Edit post</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
              <div class="col-sm-12">
                <div class="box-body">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Name</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="inputName" placeholder="Name">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Title</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="inputName" placeholder="Title">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Slug</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="inputName" placeholder="Slug">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Price</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="inputName" placeholder="Price">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Zip</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="inputName" placeholder="Zip">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Country</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="inputName" placeholder="Country">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Address</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="inputName" placeholder="Addess">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Phone No.</label>

                    <div class="col-sm-9">
                      <input type="email" class="form-control" id="inputEmail3" placeholder="Phone No.">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Textarea</label>
                    <div class="col-sm-9">
                      <textarea class="form-control" rows="3" placeholder="Enter ..." data-gramm="true" data-txt_gramm_id="468e8b02-5c8a-6217-d5b6-2c9596d9b903" data-gramm_id="468e8b02-5c8a-6217-d5b6-2c9596d9b903" spellcheck="false" data-gramm_editor="true"></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-9">
                      <label for="exampleInputFile">File input</label>
                      <input type="file" id="exampleInputFile">

                      <p class="help-block">Not more then 2mb</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox"> Remember me
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
                <!-- /.box-footer -->
            </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<!-- Modal -->
<div class="modal fade in" id="modal-view">
  <div class="modal-dialog postedit">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">View post</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
              <div class="col-sm-12">
                
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Name :</label>

                    <div class="col-sm-9">
                      <p class="view-info"> Nina Mcintire</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Title :</label>

                    <div class="col-sm-9">
                      <p class="view-info"> Lorem</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Slug :</label>

                    <div class="col-sm-9">
                      <p class="view-info"> Mohali</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Price :</label>

                    <div class="col-sm-9">
                      <p class="view-info"> $35</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Zip :</label>

                    <div class="col-sm-9">
                      <p class="view-info"> 145022</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Country :</label>

                    <div class="col-sm-9">
                      <p class="view-info"> India</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Address :</label>

                    <div class="col-sm-9">
                      <p class="view-info"> mohali, India</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Phone No. :</label>
                    <div class="col-sm-9">
                      <p class="view-info"> 1234567890</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Textarea :</label>
                    <div class="col-sm-9">
                      <p class="view-info">Lorem ipsum represents a long-held tradition for designers, typographers and the like. Some people hate it and argue for its demise, but others ignore the hate as they create awesome tools to help create filler text for everyone from bacon lovers to Charlie Sheen fans.</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Image <span class="dott">:</span></label>
                    <div class="col-sm-9">
                      <img src="./public/images/default-50x50.gif" alt="" class="profile-image">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox"> Remember me
                        </label>
                      </div>
                    </div>
                  </div>
             
              </div>
              
                <!-- /.box-footer -->
            </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<style>
 .tableicon {padding-right: 10px;}
 .view-info {
    margin: 0px;
    padding-top: 7px;
    color: #555;
}
.modal-body {
    overflow: hidden;
}
.postedit .control-label {
    text-align: left;
}
.view-info {
    margin: 0px;
    padding-top: 7px;
    color: #555;
}
</style>
    @endsection

